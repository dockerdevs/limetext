FROM debian:sid-slim

MAINTAINER Naresh Mehta

# Proper build instructions available on
# https://github.com/limetext/lime/wiki/Building-on-Ubuntu-16.04
# and https://github.com/limetext/lime/wiki/Building

# Install depdencies for building limetext
USER root
RUN apt-get update && apt-get install -y build-essential apt-utils \
	git golang libonig-dev libonig2 mercurial python3.5 \
	python3.5-dev libqt5opengl5-dev qml-module-qtgraphicaleffects \
	qtbase5-private-dev qtdeclarative5-dev \
	qtdeclarative5-dev  qml-module-qtquick-window2 \
	qml-module-qtquick2 qml-module-qtquick-layouts

# Set the proper environment variables
ENV GOPATH=/
ENV PKG_CONFIG_PATH=$GOPATH/src/github.com/limetext/rubex
ENV GODEBUG=0
ENV cgocheck=0

EXPOSE 8080

# All done. Lets create entry point needed
RUN mkdir -p /opt
ADD initd.sh /opt/
ADD runImage.sh /opt/
RUN chmod +x /opt/initd.sh && chmod +x /opt/runImage.sh && \
	./opt/initd.sh
ENTRYPOINT ["/opt/runImage.sh"]