#!/bin/bash

echo "Welcome to the limeText Run Environment"
echo "Follow me on Twitter @wolverine2k"
echo "!ENJOY!"

export GOPATH=/

# Setup the QML frontend alias
alias limeqml="cd $GOPATH/src/github.com/limetext/lime-qml/main && go build && ./main"

# Setup Termbox frontend alias
alias limeterm="cd $GOPATH/src/github.com/limetext/lime-termbox/main && go build && ./main main.go"

# Setup the HTML frontend alias
alias limehtml="cd $GOPATH/src/github.com/limetext/lime-html/main && go build"