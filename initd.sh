#!/bin/bash

echo "Welcome to the limeText Run Environment"
echo "Follow me on Twitter @wolverine2k"
echo "!ENJOY!"

export GOPATH=/

# Fetch respective backend and frontends
cd $GOPATH
go get github.com/limetext/sublime
go get github.com/limetext/backend
go get github.com/limetext/lime-qml/main/...
go get github.com/limetext/lime-termbox/main/...
	
# Compile backend	
cd $GOPATH/src/github.com/limetext/backend
git submodule update --init --recursive
go test github.com/limetext/backend/...

# Setup the QML frontend
cd $GOPATH/src/github.com/limetext/lime-qml
git submodule update --init --recursive

# Setup the Termbox frontend
cd $GOPATH/src/github.com/limetext/lime-termbox
git submodule update --init --recursive

# Setup the QML frontend alias
alias limeqml="cd $GOPATH/src/github.com/limetext/lime-qml/main && go build && ./main"

# Setup Termbox frontend alias
alias limeterm="cd $GOPATH/src/github.com/limetext/lime-termbox/main && go build && ./main main.go"

# Setup the HTML frontend alias
alias limehtml="cd $GOPATH/src/github.com/limetext/lime-html/main && go build"